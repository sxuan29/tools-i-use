## List of tools set up for Mac that I use

1. Sublime 3
    - shortcut command
    - packages
        - markdown preview
        
2. Intellij CE

3. Pyenv

4. Iterm && Oh my zsh

5. Git && PreCommit && ShellCheck

6. Docker

7. GcloudSDK

8. SDKMan

9. KeyBase

10. Slack

11. Aws Cli

12. Ssh Key Config
